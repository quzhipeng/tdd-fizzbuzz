export type FizzBuzzType = number | string;

const fizzBuzzConverter = (number: number): FizzBuzzType => {
    let str = ''
    if(String(number).indexOf('3')>-1){
        str = 'Fizz'
    }else {
        if (number % 3 === 0) {
            str += 'Fizz'
        }
        if(number % 5 === 0){
            str += 'Buzz'
        }
        if(number % 7 === 0){
            str += 'Whizz'
        }
    }
    if(!str){
        str = String(number)
    }
    return str;
}

export default fizzBuzzConverter;