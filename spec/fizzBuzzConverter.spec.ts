import fizzBuzzConverter, {FizzBuzzType} from '../src/fizzBuzzConverter'


describe('should convert to fizz when 3 multiple', () => {
    let result: FizzBuzzType = 0;

    beforeEach(() => {
        result = 0;
    });
    test('number is 6', () => {
        result = fizzBuzzConverter(6)
    });
    test('number is 12', () => {
        result = fizzBuzzConverter(12)
    });
    test('number is 18', () => {
        result = fizzBuzzConverter(18)
    });
    test('number is 99', () => {
        result = fizzBuzzConverter(99)
    });

    afterEach(() => {
        expect(result).toBe("Fizz");
    });
});

describe('should convert to buzz when 5 multiple', () => {
    let result: FizzBuzzType = 0;

    beforeEach(() => {
        result = 0;
    });
    afterEach(() => {
        expect(result).toBe("Buzz");
    });

    test('number is 5', () => {
        result = fizzBuzzConverter(5)
    });

    test('number is 10', () => {
        result = fizzBuzzConverter(10)
    });

    test('number is 20', () => {
        result = fizzBuzzConverter(20)
    });

    test('number is 85', () => {
        result = fizzBuzzConverter(85)
    });

    test('number is 95', () => {
        result = fizzBuzzConverter(95)
    });

    test('number is 100', () => {
        result = fizzBuzzConverter(100)
    });
});

describe('should convert to buzz when 7 multiple', () => {
    let result: FizzBuzzType = 0;

    beforeEach(() => {
        result = 0;
    });
    afterEach(() => {
        expect(result).toBe("Whizz");
    });

    test('number is 7', () => {
        result = fizzBuzzConverter(7)
    });

    test('number is 14', () => {
        result = fizzBuzzConverter(14)
    });

    test('number is 28', () => {
        result = fizzBuzzConverter(28)
    });

    test('number is 49', () => {
        result = fizzBuzzConverter(49)
    });
});
describe('should convert to FizzBuzz when 15 multiple', () => {
    let result: FizzBuzzType = 0;

    beforeEach(() => {
        result = 0;
    });
    afterEach(() => {
        expect(result).toBe("FizzBuzz");
    });

    test('number is 15', () => {
        result = fizzBuzzConverter(15)
    });
    test('number is 45', () => {
        result = fizzBuzzConverter(45)
    });

    test('number is 60', () => {
        result = fizzBuzzConverter(60)
    });

    test('number is 75', () => {
        result = fizzBuzzConverter(75)
    });
    test('number is 90', () => {
        result = fizzBuzzConverter(90)
    });
    test('number is 120', () => {
        result = fizzBuzzConverter(120)
    });

});
describe('should convert to FizzBuzz when 21 multiple', () => {
    let result: FizzBuzzType = 0;

    beforeEach(() => {
        result = 0;
    });
    afterEach(() => {
        expect(result).toBe("FizzWhizz");
    });

    test('number is 21', () => {
        result = fizzBuzzConverter(21)
    });

    test('number is 42', () => {
        result = fizzBuzzConverter(42)
    });

    test('number is 84', () => {
        result = fizzBuzzConverter(84)
    });
    test('number is 126', () => {
        result = fizzBuzzConverter(126)
    });
});
describe('should convert to FizzBuzz when 35 multiple', () => {
    let result: FizzBuzzType = 0;

    beforeEach(() => {
        result = 0;
    });
    afterEach(() => {
        expect(result).toBe("BuzzWhizz");
    });
    test('number is 70', () => {
        result = fizzBuzzConverter(70)
    });

    test('number is 140', () => {
        result = fizzBuzzConverter(140)
    });

    test('number is 175', () => {
        result = fizzBuzzConverter(175)
    });
    test('number is 245', () => {
        result = fizzBuzzConverter(245)
    });

});
describe('should convert to FizzBuzz when 105 multiple', () => {
    let result: FizzBuzzType = 0;

    beforeEach(() => {
        result = 0;
    });
    afterEach(() => {
        expect(result).toBe("FizzBuzzWhizz");
    });

    test('number is 105', () => {
        result = fizzBuzzConverter(105)
    });

    test('number is 210', () => {
        result = fizzBuzzConverter(210)
    });

    test('number is 420', () => {
        result = fizzBuzzConverter(420)
    });
    test('number is 525', () => {
        result = fizzBuzzConverter(525)
    });

});
describe('should convert to FizzBuzz when include 3 ', () => {
    let result: FizzBuzzType = 0;

    beforeEach(() => {
        result = 0;
    });
    afterEach(() => {
        expect(result).toBe("Fizz");
    });

    test('number is 3', () => {
        result = fizzBuzzConverter(3)
    });

    test('number is 13', () => {
        result = fizzBuzzConverter(13)
    });

    test('number is 23', () => {
        result = fizzBuzzConverter(23)
    });

    test('number is 63', () => {
        result = fizzBuzzConverter(63)
    });

});
describe('should convert to self string when common number', () => {
    let result: FizzBuzzType = 0;

    beforeEach(() => {
        result = 0;
    });
    afterEach(() => {
        expect(result).toBe(result.toString());
    });

    test('number is 1', () => {
        result = fizzBuzzConverter(1)
    });

    test('number is 2', () => {
        result = fizzBuzzConverter(2)
    });

    test('number is 4', () => {
        result = fizzBuzzConverter(4)
    });

    test('number is 94', () => {
        result = fizzBuzzConverter(94)
    });

    test('number is 97', () => {
        result = fizzBuzzConverter(97)
    });
});
