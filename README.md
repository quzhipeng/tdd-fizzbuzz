# 作业 FizzBuzz Basic

## 开发环境
 - node v14
 - TypeScript
 - Jest
 
## 题目背景 
你是一名体育老师，在某次课距离下课还有五分钟时，你决定搞一个游戏。此时有N名学生在上 课。游戏的规则是:
1.	让所有学生排成一队，然后按顺序报数。
2.	学生报数时，如果所报数字是3的倍数，那么不能说该数字，而要说Fizz;如果所报数字是5 的倍数，那么要说Buzz;如果所报数字是第7的倍数，那么要说Whizz。
3.	学生报数时，如果所报数字同时是两个特殊数的倍数情况下，也要特殊处理，比如3和5的 倍数，那么不能说该数字，而是要说FizzBuzz, 以此类推。如果同时是三个特殊数的倍数， 那么要说FizzBuzzWhizz。
4.	学生报数时，如果所报数字包含了3，那么也不能说该数字，而是要说相应的单词，比如要 报13的同学应该说Fizz。
5.	如果数字中包含了3，那么忽略规则2和规则3，比如要报30的同学只报Fizz，不报FizzBuzz 。
### FizzBuzz Basic
1. 打印字符串`"Fizz"`
   - Given 一个数字
   - When 该数字为3的倍数
   - then 打印 `"Fizz"`
2. 打印字符串`"Buzz"`
   - Given 一个数字
   - When 该数字为5的倍数
   - then 打印 `"Buzz"`
3. 打印字符串`"Whizz"`
   - Given 一个数字
   - When 该数字为7的倍数
   - then 打印`"Whizz"`
4. 打印字符串`"FizzBuzz"`
   - Given 一个数字
   - When 该数字为3和5的倍数
   - then 打印`"FizzBuzz"`
5. 打印字符串`"FizzWhizz"`
   - Given 一个数字
   - When 该数字为3和7的倍数
   - then 打印`"FizzWhizz"`
6. 打印字符串`"BuzzWhizz"`
   - Given 一个数字
   - When 该数字为5和7的倍数
   - then 打印`"BuzzWhizz"`
7. 打印字符串`"FizzBuzzWhizz"`
   - Given 一个数字
   - When 该数字同时为3、5、7的倍数
   - then 打印`"FizzBuzzWhizz"`
8. 打印字符串`"Fizz"`
   - Given 一个数字
   - When 该数数字包含3 
   - then 忽略其他规则只打印字符串`"Fizz"`
9. 打印数字本身
   - Given 一个数字
   - When 不符合以上规则
   - then 打印将数字转为字符串打印出来
10. 异常处理
     - Given 非数字
     - When 判断不是数字
     - then 抛出异常

## Tasking
### 数字过滤方法
| Task | Input | Output |
|:---|:---|:---|
| 1 | 6, 12, 21, 99 |  `"Fizz"` |
| 2 | 5, 10, 20, 85, 95, 100 |  `"Buzz"` |
| 3 | 7, 14, 28, 49 |  `"Whizz"` |
| 4 | 15, 45, 60, 75, 90, 120 |  `"FizzBuzz"` |
| 5 | 21, 42, 84, 126 |  `"FizzWhizz"` |
| 6 | 70, 140, 175,245 |  `"BuzzWhizz"` |
| 7 | 105, 210, 420,525 |  `"FizzBuzzWhizz"` |
| 8 | 3, 13, 23, 63 |  `"Fizz"` |
| 4 | 1, 2, 4, 94, 97|  `"1"`, `"2"`, `"4"`, `"94"`, `"97"` |
